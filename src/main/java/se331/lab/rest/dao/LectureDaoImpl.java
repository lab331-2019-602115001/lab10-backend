package se331.lab.rest.dao;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;
import se331.lab.rest.entity.Lecturer;

import java.util.List;

@Repository
@Profile("noDao")
public class LectureDaoImpl implements LecturerDao{
    List<Lecturer> lecturers;
    @Override
    public List<Lecturer> getAllLecturer() {
        return lecturers;
    }

    @Override
    public Lecturer findById(Long lecturerId) {
        return null;
    }

    @Override
    public Lecturer saveLecturer(Lecturer lecturer) {
        lecturer.setId((long) lecturers.size());
        lecturers.add(lecturer);
        return lecturer;
    }
}
