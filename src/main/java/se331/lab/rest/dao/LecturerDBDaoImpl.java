package se331.lab.rest.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;
import se331.lab.rest.entity.Lecturer;
import se331.lab.rest.repository.LecturerRepository;

import java.util.List;

@Repository
@Profile("dbDao2")
public class LecturerDBDaoImpl implements LecturerDao{

    @Autowired
    LecturerRepository lecturerRepository;

    @Override
    public List<Lecturer> getAllLecturer() {
        return lecturerRepository.findAll();
    }

    @Override
    public Lecturer findById(Long lecturerId) {
        return null;
    }

    @Override
    public Lecturer saveLecturer(Lecturer lecturer) {
        return null;
    }
}
