package se331.lab.rest.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import se331.lab.rest.dao.LecturerDao;
import se331.lab.rest.entity.Lecturer;

import java.util.List;

@Service
public class LecturerServiceImpl implements LecturerService{
    @Autowired
    LecturerDao lecturerDao;

    @Override
    public List<Lecturer> getAllLecturer() {
        List<Lecturer> lecturers = lecturerDao.getAllLecturer();
        return lecturers;
    }

    @Override
    public Lecturer findById(Long lecturerId) {
        return null;
    }

    @Override
    public Lecturer saveLecturer(Lecturer lecturer) {
        return null;
    }
}
